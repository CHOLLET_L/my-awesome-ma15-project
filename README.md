
 # N-Body Simulator

Simple N-Body problem simulator, with Python and GTK

Possibility to use diverse numeric method:
   + Euler
   + Euler with Richardson extrapolation
   + Runge-Kutta (order 2 and 4)


The file [GTK.py](GTK.py) is the version with a GUI  and [Sim.py](Sim.py) is just a simple script without any user interface.

This project is a student project, and it was intended for french teachers. So, comments and the GUI are both written in french.