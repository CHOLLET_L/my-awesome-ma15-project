import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D as ax3d


########## Constantes ##########

G = 2.959130713485796*10**-4
Ms = 1  # 1.9891*10**30 kg
Mt = (5.9722*10**24) / (1.9891*10**30)
Ml = (7.3477*10**22) / (1.9891*10**30)

ech = 10**9
dt = 1

"""On définit chacun de nos corps comme un tableau avec, dans l'ordre des colonnes, la masse, le vecteur position 
initiale, le vecteur vitesse initiale"""

soleil = np.array([Ms, np.array([0, 0, 0]), np.array([0, 0, 0])])
terre = np.array([Mt, np.array([-0.855642364986048, -0.519441624074917, -0.000102075036916006]),
                  np.array([0.0086251172708209, -0.0147835760144126, -0.0000011159613677806])])
lune = np.array([Ml, np.array([-0.855145529856587, -0.516801998416403, 0.000137928287597256]),
        np.array([0.00807486408995559, -0.0146693081929257, 9.52735497245847E-06])])

"""Notre système est un tableau composé de tout les corps que l'on veut afficher"""
systeme = [soleil, terre]
n_corps = len(systeme)

########## Méthodes numérique ##########


def Euler(systeme_t):
    "Méthode de Euler"
    acc_t0 = acc(systeme_t)
    l1 = []
    for i in range(n_corps):
        l1.append([0, systeme_t[i][2], acc_t0[i]])
    k1 = np.array(l1)

    next_systeme = systeme_t + dt * k1
    return next_systeme


def RK2(systeme_t):
    "Méthode de Runge-Kutta d'ordre 2"
    acc_t0 = acc(systeme_t)
    l1 = []
    for i in range(n_corps):
        l1.append([0, systeme_t[i][2], acc_t0[i]])
    k1 = np.array(l1)

    systeme_t_half = systeme_t + dt * k1
    acc_t_half = acc(systeme_t_half)
    l2 = []
    for i in range(n_corps):
        l2.append([0, systeme_t_half[i][2], acc_t_half[i]])
    k2 = np.array(l2)

    next_systeme = systeme_t + dt * k1/2 + dt * k2/2
    return next_systeme


def ER(systeme_t):
    "Méthode de Euler-Richardson"
    acc_t0 = acc(systeme_t)
    l1 = []
    for i in range(n_corps):
        l1.append([0, systeme_t[i][2], 0])
    k1 = np.array(l1)

    systeme_t_half = systeme_t + dt/2 * k1
    acc_t_half = acc(systeme_t_half)
    l2 = []
    for i in range(n_corps):
        l2.append([0, systeme_t[i][2], acc_t0[i]])
    next_systeme = np.copy(systeme_t)
    for i in range(n_corps):
        next_systeme[i][1] = systeme_t[i][1] + systeme_t[i][2] * dt + acc_t0[i] * (dt**2)/2
        next_systeme[i][2] = systeme_t[i][2] + acc_t_half[i] * dt
    return next_systeme


def RK4(systeme_t):
    "Méthode de Runge-Kutta d'ordre 4"
    acc_t0 = acc(systeme_t)
    l1 = []
    for i in range(n_corps):
        l1.append([0, systeme_t[i][2], acc_t0[i]])
    k1 = np.array(l1)

    systeme_t_half_1 = systeme_t + dt * k1 / 2
    acc_t_half_1 = acc(systeme_t_half_1)
    l2 = []
    for i in range(n_corps):
        l2.append([0, systeme_t_half_1[i][2], acc_t_half_1[i]])
    k2 = np.array(l2)

    systeme_t_half_2 = systeme_t + dt * k2 / 2
    acc_t_half_2 = acc(systeme_t_half_2)
    l3 = []
    for i in range(n_corps):
        l3.append([0, systeme_t_half_2[i][2], acc_t_half_2[i]])
    k3 = np.array(l3)

    systeme_t1 = systeme_t + dt * k3
    acc_t1 = acc(systeme_t1)
    l4 = []
    for i in range(n_corps):
        l4.append([0, systeme_t1[i][2], acc_t1[i]])
    k4 = np.array(l4)

    next_systeme = systeme_t + dt * (k1 + 2*k2 + 2*k3 + k4) / 6
    return next_systeme


########## Calcul des forces du système ##########


def force_g(mb, dist):
    """Fonction qui retourne l'accélération subit par le corps en fonction de sa masse et de sa distance"""
    return G*mb/(np.linalg.norm(dist)**2)


def acc(systeme_local):
    """Fonction qui retourne la somme des forces subit par chaque corps dans un tableau force_sum"""
    force = [[(0, 0, 0) for i in range(n_corps)] for j in range(n_corps)]
    force_sum = [(0, 0, 0) for i in range(n_corps)]

    for i in range(n_corps):  # On balaye la liste pour trouver l'accélération subit par chaque planète
        for j in range(n_corps): # On balaye la liste pour trouver l'accélération que provoque chaque planète sur la première
            if i == j:
                force[i][j] = 0, 0, 0
                continue
            d = systeme_local[j][1] - systeme_local[i][1]
            u_rho = d / np.linalg.norm(d)  # notre vecteur unitaire
            norme_f = force_g(systeme_local[j][0], np.linalg.norm(d))  # calcul de la norme de f
            force[i][j] = norme_f * u_rho
            force_sum[i] = force_sum[i] + force[i][j]

    return force_sum  # on retourne un tableau avec les valeures des accélérations subit par chaque planète


########## Affichage des graphs ##########


def graph_3d_mov(systeme_t, nbr_jour, meth):
    """Fonction pour afficher le graphique 3d en mouvement du systeme"""
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    for j in range(int(nbr_jour/dt)):
        ax.set_xlim3d(-2, 2)
        ax.set_ylim3d(-2, 2)
        ax.set_zlim3d(-2, 2)

        for i in range(0, len(systeme_t)): # on affiche toute les planètes du système
            ax.scatter3D(systeme_t[i][1][0], systeme_t[i][1][1], systeme_t[i][1][2], c='y')


        plt.pause(0.1)
        plt.cla()
        if meth == "E": # Choix de la méthode
            systeme_t = Euler(systeme_t)
        elif meth == "RK2":
            systeme_t = RK2(systeme_t)
        elif meth == "RK4":
            systeme_t = RK4(systeme_t)
        elif meth == "ER":
            systeme_t = ER(systeme_t)


def graph_3d(systeme_t, nbr_jour, meth):
    """Fonction pour afficher le graphique 3d avec les trajectoire des corps du système"""
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    for j in range(int(nbr_jour/dt)):
        ax.set_xlim3d(-2, 2)
        ax.set_ylim3d(-2, 2)
        ax.set_zlim3d(-2, 2)

        for i in range(0, len(systeme_t)):  #  on affiche toute les planètes du système
            ax.scatter3D(systeme_t[i][1][0], systeme_t[i][1][1], systeme_t[i][1][2], c='y')

        if meth == "E":
            systeme_t = Euler(systeme_t)
        elif meth == "RK2":
            systeme_t = RK2(systeme_t)
        elif meth == "RK4":
            systeme_t = RK4(systeme_t)
        elif meth == "ER":
            systeme_t = ER(systeme_t)
    plt.show()


def graph_2d(systeme_t, nbr_jour, meth):
    """Fonction pour afficher le graphique 2d avec les trajectoires des corps du système"""
    fig = plt.figure()
    ax = plt.axes()
    plt.scatter(systeme_t[0][1][0], systeme_t[0][1][1], marker='.')
    for j in range(int(nbr_jour/dt)):
        ax.set_xlim(-2, 2)
        ax.set_ylim(-2, 2)
        for i in range(0, len(systeme_t)):  #  on affiche toute les planètes du système
            plt.scatter(systeme_t[i][1][0], systeme_t[i][1][1], marker=',', s=1, c='r')
        if meth == "E":
            systeme_t = Euler(systeme_t)
        elif meth == "RK2":
            systeme_t = RK2(systeme_t)
        elif meth == "RK4":
            systeme_t = RK4(systeme_t)
        elif meth == "ER":
            systeme_t = ER(systeme_t)
    plt.show()


"""On affiche les graphiques : on choisit la méthode : 
E ==> Euler
ER ==> Euler-Richardson
RK2 ==> Runge-Kutta 2
RK4 ==> Runge-Kutta 4"""

graph_2d(systeme, 365, "E")
graph_3d(systeme, 365, "RK2")
graph_3d_mov(systeme, 365, "RK4")
