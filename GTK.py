import numpy as np

import gi
gi.require_version("Gtk", "3.0")
gi.require_version("Gdk", "3.0")
from gi.repository import Gtk
from gi.repository import Gdk

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D as ax3d

checkbox_dic = {"E": False,
                "ER": False,
                "RK2": False,
                "RK4": False
                }

G = 2.959130713485796*10**-4
Ms = 1.9891e+30

SystemeSolaire = [["Soleil", "1.9885E+30", "-1.984148925210301E-03", "7.610792063004368E-03", "-2.559358684023067E-05",
                   "-8.380854394655522E-06", "3.682339859937859E-07", "2.167379738347977E-07"],
                  ["Mercure", "3.302E+23", "-1.547339992324320E-01", "2.876235701973106E-01", "3.686812518410502E-02",
                   "-3.036913531119162E-02", "-1.240138324932380E-02", "1.772017243209204E-03"],
                  ["Venus", "4.8685E+24", "6.802254980956648E-01", "2.506999720237926E-01", "-3.605872899325935E-02",
                   "-6.865430290034395E-03", "1.896417967361841E-02", "6.561276764739633E-04"],
                  ["Terre", "5.97219E+24", "-3.649076159953604E-01", "-9.390032393720060E-01", "2.139333952434441E-05",
                   "1.577935252017643E-02", "-6.229175248322921E-03", "3.280955943093282E-07"],
                  ["Mars", "6.4171E+23", "-7.570187618430400E-01", "1.450727472854759E+00", "4.873874657497723E-02",
                   "-1.187936211800590E-02", "-5.295981340789212E-03", "1.805109588597094E-04"],
                  ["Jupiter", "1.89813E+27", "-1.07186913078189E+00", "-5.186123486568133E+00", "4.548533044567648E-02",
                   "7.298981168824481E-03", "-1.167024622203780E-03", "-1.584086969759099E-04"],
                  ["Saturne", "5.6834E+26", "2.726191239986094E+00", "-9.667119148569240E+00", "5.956575031110661E-02",
                   "5.061422322233009E-03", "1.496289062888281E-03", "-2.271556342967825E-04"],
                  ["Uranus", "8.6813E+25", "1.669934702473013E+01", "1.072395101343042E+01", "-1.765132153066890E-01",
                   "-2.154213372502666E-03", "3.126173665621770E-03", "3.958855572353032E-05"],
                  ["Neptune", "1.02413E+26", "2.909259180673609E+01", "-7.021574213246689E+00", "-5.258718987451909E-01",
                   "7.155858205076067E-04", "3.070537730425259E-03", "-7.933623549556279E-05"],
                  ["Pluton", "1.307E+22", "1.233168923660697E+01", "-3.147151418327766E+01", "-1.994000964976144E-01",
                   "3.002931821440962E-03", "4.868193644924224E-04", "-9.242048047839543E-04"]]


def plotter(cells_content):
    systeme = cells_content[0]
    method = cells_content[1]
    dt = cells_content[2]
    t_final = cells_content[3]*365
    if cells_content[4] == "2d":
        graph_2d(systeme, t_final, method, dt)
    elif cells_content[4] == "3d":
        graph_3d(systeme, t_final, method, dt)
    elif cells_content[4] == "3dan":
        graph_3dan(systeme, t_final, method, dt)


def graph_2d(systeme_t, nbr_jour, meth, dt):
    fig = plt.figure()
    ax = plt.axes()
    plt.scatter(0, 0, marker='o')
    System = [systeme_t]
    nb_boucles = int(nbr_jour / dt)
    for j in range(nb_boucles):
        progressbar_watchdog(j, nb_boucles)
        if meth == "E":
            System.append(Euler(System[j], dt))
        elif meth == "RK2":
            System.append(RK2(System[j], dt))
        elif meth == "RK4":
            System.append(RK4(System[j], dt))
        elif meth == "ER":
            System.append(ER(System[j], dt))

    for i in range(1, len(systeme_t)):
        plt.scatter([System[j][i][1][0]-System[j][0][1][0] for j in range(len(System))],
                    [System[j][i][1][1]-System[j][0][1][1] for j in range(len(System))], marker=',', s=1)

    iter_f = data_store.get_iter_first()
    legend = []
    while iter_f is not None:
        legend.append(data_store[iter_f][0])
        # print(data_store[iter_f][0])
        iter_f = data_store.iter_next(iter_f)
    plt.legend(legend)

    plt.show()


def graph_3d(systeme_t, nbr_jour, meth, dt):
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.scatter3D(0, 0, 0, marker='o')

    System = [systeme_t]
    nb_boucles = int(nbr_jour / dt)
    for j in range(nb_boucles):
        progressbar_watchdog(j, nb_boucles)
        if meth == "E":
            System.append(Euler(System[j], dt))
        elif meth == "RK2":
            System.append(RK2(System[j], dt))
        elif meth == "RK4":
            System.append(RK4(System[j], dt))
        elif meth == "ER":
            System.append(ER(System[j], dt))
    for i in range(1, len(systeme_t)):
        ax.scatter3D([System[j][i][1][0]-System[j][0][1][0] for j in range(len(System))],
                     [System[j][i][1][1]-System[j][0][1][1] for j in range(len(System))],
                     [System[j][i][1][2]-System[j][0][1][2] for j in range(len(System))], marker=',', s=1)
    # ax.set_xlim3d(-2, 2)
    # ax.set_ylim3d(-2, 2)
    ax.set_zlim3d(-2, 2)

    iter_f = data_store.get_iter_first()
    legend = []
    while iter_f is not None:
        legend.append(data_store[iter_f][0])
        # print(data_store[iter_f][0])
        iter_f = data_store.iter_next(iter_f)
    plt.legend(legend)

    plt.show()


def graph_3dan(systeme_t, nbr_jour, meth, dt):
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    System = [systeme_t]
    nb_boucles = int(nbr_jour / dt)
    for j in range(nb_boucles):
        progressbar_watchdog(j, nb_boucles)
        if meth == "E":
            System.append(Euler(System[j], dt))
        elif meth == "RK2":
            System.append(RK2(System[j], dt))
        elif meth == "RK4":
            System.append(RK4(System[j], dt))
        elif meth == "ER":
            System.append(ER(System[j], dt))

        ax.scatter3D(0, 0, 0, marker='o')
        for i in range(1, len(systeme_t)):
            ax.scatter3D(System[j][i][1][0]-System[j][0][1][0],
                         System[j][i][1][1]-System[j][0][1][1],
                         System[j][i][1][2]-System[j][0][1][2], marker='o')

        d_max = distance_max(System[j])
        ax.set_xlim3d(-d_max, d_max)
        ax.set_ylim3d(-d_max, d_max)
        ax.set_zlim3d(-2, 2)
        plt.pause(0.001)
        plt.cla()

    plt.show()


def distance_max(system_t):
    d_max = np.linalg.norm(system_t[0][1])
    for planet in system_t:
        d = np.linalg.norm(planet[1])
        if d > d_max:
            d_max = d
    return d_max


def force_g(mb, dist):
    return G*mb/(np.linalg.norm(dist)**2)


def acc(systeme_local):
    force = [[(0, 0, 0) for i in range(len(systeme_local))] for j in range(len(systeme_local))]
    force_sum = [(0, 0, 0) for i in range(len(systeme_local))]

    for i in range(len(systeme_local)):
        for j in range(len(systeme_local)):
            if i == j:
                force[i][j] = 0, 0, 0
                continue
            d = systeme_local[j][1] - systeme_local[i][1]
            u_rho = d / np.linalg.norm(d)
            norme_f = force_g(systeme_local[j][0], np.linalg.norm(d))
            force[i][j] = norme_f * u_rho
            force_sum[i] = force_sum[i] + force[i][j]

    return force_sum


def Euler(systeme_t, dt):
    acc_t0 = acc(systeme_t)
    l1 = []
    for i in range(len(systeme_t)):
        l1.append([0, systeme_t[i][2], acc_t0[i]])
    k1 = np.array(l1)

    next_systeme = systeme_t + dt * k1
    return next_systeme


def RK2(systeme_t, dt):

    acc_t0 = acc(systeme_t)
    l1 = []
    for i in range(len(systeme_t)):
        l1.append([0, systeme_t[i][2], acc_t0[i]])
    k1 = np.array(l1)

    systeme_t_half = systeme_t + dt * k1
    acc_t_half = acc(systeme_t_half)
    l2 = []
    for i in range(len(systeme_t)):
        l2.append([0, systeme_t_half[i][2], acc_t_half[i]])
    k2 = np.array(l2)

    next_systeme = systeme_t + dt * k1/2 + dt * k2/2
    return next_systeme


def ER(systeme_t, dt):
    acc_t0 = acc(systeme_t)
    l1 = []
    for i in range(len(systeme_t)):
        l1.append([0, systeme_t[i][2], 0])
    k1 = np.array(l1)

    systeme_t_half = systeme_t + dt/2 * k1
    acc_t_half = acc(systeme_t_half)
    l2 = []
    for i in range(len(systeme_t)):
        l2.append([0, systeme_t[i][2], acc_t0[i]])
    next_systeme = np.copy(systeme_t)
    for i in range(len(systeme_t)):
        next_systeme[i][1] = systeme_t[i][1] + systeme_t[i][2] * dt + acc_t0[i] * (dt**2)/2
        next_systeme[i][2] = systeme_t[i][2] + acc_t_half[i] * dt
    return next_systeme


def RK4(systeme_t, dt):
    acc_t0 = acc(systeme_t)
    l1 = []
    for i in range(len(systeme_t)):
        l1.append([0, systeme_t[i][2], acc_t0[i]])
    k1 = np.array(l1)

    systeme_t_half_1 = systeme_t + dt * k1 / 2
    acc_t_half_1 = acc(systeme_t_half_1)
    l2 = []
    for i in range(len(systeme_t)):
        l2.append([0, systeme_t_half_1[i][2], acc_t_half_1[i]])
    k2 = np.array(l2)

    systeme_t_half_2 = systeme_t + dt * k2 / 2
    acc_t_half_2 = acc(systeme_t_half_2)
    l3 = []
    for i in range(len(systeme_t)):
        l3.append([0, systeme_t_half_2[i][2], acc_t_half_2[i]])
    k3 = np.array(l3)

    systeme_t1 = systeme_t + dt * k3
    acc_t1 = acc(systeme_t1)
    l4 = []
    for i in range(len(systeme_t)):
        l4.append([0, systeme_t1[i][2], acc_t1[i]])
    k4 = np.array(l4)

    next_systeme = systeme_t + dt * (k1 + 2*k2 + 2*k3 + k4) / 6
    return next_systeme


def get_from_cells(graph):
    dt = float(entry_dt.get_text())
    tf = float(entry_tf.get_text())
    method = None
    # if graph != '2d':
    for i in checkbox_dic:
        if checkbox_dic[i] is True:
            method = i
            break
    if method is None:
        print("Pas de méthode numérique spécifié :")
        print("Utilisation de RK4")
        method = 'RK4'
    # else:
    #     for i in checkbox_dic:
    #         if checkbox_dic[i] is True:
    #             if method is None:
    #                 method = i
    #             else:
    #                 if type(method) == type(str("method")):
    #                     method = [method]
    #                 method.append(i)
    #     if method is None:
    #         print("Pas de méthode numérique spécifié :")
    #         print("Utilisation de RK4")
    #         method = 'RK4'

    iter_f = data_store.get_iter_first()
    systeme = []
    while iter_f is not None:
        planet = [float(data_store[iter_f][1])/Ms,
                  np.array([float(data_store[iter_f][2]), float(data_store[iter_f][3]), float(data_store[iter_f][4])]),
                  np.array([float(data_store[iter_f][5]), float(data_store[iter_f][6]), float(data_store[iter_f][7])])]
        systeme.append(planet)
        iter_f = data_store.iter_next(iter_f)
    return [np.array(systeme), method, dt, tf, graph]


def checkbox_watchdog(name, value):
    checkbox_dic[name] = value
    a = 0
    for i in checkbox_dic:
        if checkbox_dic[i] is True:
            a += 1
    if a > 1:
        bt_2d.set_sensitive(False)
        bt_3d.set_sensitive(False)
        bt_3d_an.set_sensitive(False)
    else:
        bt_2d.set_sensitive(True)
        bt_3d.set_sensitive(True)
        bt_3d_an.set_sensitive(True)


def progressbar_watchdog(pos, val_max):
    if (pos == val_max-1) or (pos / val_max) >= 1:
        progress.set_fraction(0)
    else:
        progress.set_fraction(pos/val_max)
    while Gtk.events_pending():
        Gtk.main_iteration()



def edit_text_gen(nb):
    def edit_text(widget, path, text):
        data_store[path][nb] = text

    return edit_text


def Handler_key(widget, event):
    keyname = Gdk.keyval_name(event.keyval).lower()
    # print(keyname)
    if keyname == 'escape':
        Gtk.main_quit()
    if keyname == 'f11':
        left_panel.set_visible(not left_panel.get_visible())


class HandlerWin:
    def on_MyWindow_destroy(self, *args):
        Gtk.main_quit()

    def on_Button_Add_clicked(self, button):
        window_addobj.set_visible(True)

    def on_Button_Remove_clicked(self, button):
        select = treeview.get_selection()
        model, treeiter = select.get_selected()
        if treeiter is not None:
            data_store.remove(treeiter)

    def on_Button_Up_clicked(self, button):
        select = treeview.get_selection()
        model, treeiter = select.get_selected()
        data_store.swap(treeiter, data_store.iter_previous(treeiter))

    def on_Button_Down_clicked(self, button):
        select = treeview.get_selection()
        model, treeiter = select.get_selected()
        data_store.swap(treeiter, data_store.iter_next(treeiter))

    def on_Button_SySol_clicked(self, button):
        data_store.clear()
        for object in SystemeSolaire:
            data_store.append(object)

    def on_Button_Valid_clicked(self, button):
        L = []
        a = 0
        for entry in Entry:
            if a == 0:
                L.append(entry.get_text())
            else:
                L.append(float(entry.get_text()))
        data_store.append(L)

        window_addobj.set_visible(False)

    # def on_Switch_save_state_set(self, switch, state):
    #     bt_save.set_sensitive(state)
    #
    # def on_Button_save_clicked(self, button):
    #     return 0

    def on_Button_2D_clicked(self, button):
        right_panel.set_visible(False)
        plotter(get_from_cells('2d'))

    def on_Button_3D_clicked(self, button):
        right_panel.set_visible(False)
        plotter(get_from_cells('3d'))

    def on_bt_3d_an_clicked(self, button):
        right_panel.set_visible(False)
        plotter(get_from_cells('3dan'))

    def on_ck_E_toggled(self, checkbox):
        checkbox_watchdog("E", checkbox.get_active())

    def on_ck_ER_toggled(self, checkbox):
        checkbox_watchdog("ER", checkbox.get_active())

    def on_ck_RK2_toggled(self, checkbox):
        checkbox_watchdog("RK2", checkbox.get_active())

    def on_ck_RK4_toggled(self, checkbox):
        checkbox_watchdog("RK4", checkbox.get_active())


builder = Gtk.Builder()
builder.add_from_file("Sim.glade")

window = builder.get_object("MyWindow")
window_addobj = builder.get_object("Win_addobject")

builder.connect_signals(HandlerWin())

left_panel = builder.get_object("Left_panel")
right_panel = builder.get_object("Scrolled_Window")

bt_2d = builder.get_object("bt_2d")
bt_3d = builder.get_object("bt_3d")
bt_3d_an = builder.get_object("bt_3d_an")

entry_dt = builder.get_object("Entry_dt")
entry_tf = builder.get_object("Entry_Tf")

Entry = [builder.get_object("Entry_name"),
         builder.get_object("Entry_masse"),
         builder.get_object("Entry_x0"),
         builder.get_object("Entry_y0"),
         builder.get_object("Entry_z0"),
         builder.get_object("Entry_vx0"),
         builder.get_object("Entry_vy0"),
         builder.get_object("Entry_vz0")]

data_store = builder.get_object("data_store")
treeview = builder.get_object("TreeView")

progress = builder.get_object("ProgressBar")

# switch_save = builder.get_object("Switch_save")
# bt_save = builder.get_object("Button_save")

window.connect("key-press-event", Handler_key)

Cells = []
Column_Name = ["Nom", "Masse (kg)", "X0 (UA)", "Y0 (UA)", "Z0 (UA)", "Vx0 (UA/jour)", "Vy0 (UA/jour)", "Vz0 (UA/jour)"]

for i in range(8):
    Cells.append(Gtk.CellRendererText())
    Cells[i].set_property("editable", True)
    Cells[i].connect("edited", edit_text_gen(i))
    column_editabletext = Gtk.TreeViewColumn(Column_Name[i], Cells[i], text=i)
    treeview.append_column(column_editabletext)

window.show_all()
right_panel.set_visible(False)


Gtk.main()
